/*********************************************************************
*
* Software License Agreement (BSD License)
*
*  Copyright (c) 2008, Willow Garage, Inc.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of Willow Garage, Inc. nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*
* Author: Eitan Marder-Eppstein
*********************************************************************/
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>

//#include <dp_ptu47_pan_tilt_stage/PanTiltStamped.h>

#define PI 3.14159265358979323846
float pan_tf_angle=0;
float tilt_tf_angle=0;
float posx=0;
float posy=0;
float posz=0;
float orx=0;
float ory=0;
float orz=0;
float orw=1;

/*void pan_tiltCallback(const dp_ptu47_pan_tilt_stage::PanTiltStamped msg)
{
  ROS_INFO("\n Recieving pan_tf_angle = [%f] \n Recieving tilt_tf_angle = [%f]",msg.pan_angle,msg.tilt_angle);
  pan_tf_angle=msg.pan_angle;
  tilt_tf_angle=msg.tilt_angle;
}*/

void odomCallback(const nav_msgs::Odometry msg)
{
  posx=msg.pose.pose.position.x;
  posy=msg.pose.pose.position.y;
  posz=msg.pose.pose.position.z;
  orx= msg.pose.pose.orientation.x;
  ory= msg.pose.pose.orientation.y;
  orz= msg.pose.pose.orientation.z;
  orw= msg.pose.pose.orientation.w;
  
  //ROS_INFO("\n Recieving pan_tf_angle = [%f] \n Recieving tilt_tf_angle = [%f]",msg.pan_angle,msg.tilt_angle);
  //pan_tf_angle=msg.pan_angle;
  //tilt_tf_angle=msg.tilt_angle;
}

int main(int argc, char** argv){
  ros::init(argc, argv, "robot_tf_publisher");
  ros::NodeHandle n;
  ros::Subscriber odom_sub = n.subscribe<nav_msgs::Odometry>("/odom", 1, odomCallback);
  //ros::Subscriber pan_tilt_sub = n.subscribe<dp_ptu47_pan_tilt_stage::PanTiltStamped>("/dp_ptu47/pan_tilt_status_stamped", 1, pan_tiltCallback);
  tf::TransformBroadcaster broadcaster;

  ros::Rate r(10);

  while(n.ok()){
    /*broadcaster.sendTransform(
                              tf::StampedTransform(tf::Transform(tf::Quaternion(0,0,0,1), tf::Vector3(0,0.0,0.0)), 
                                                   ros::Time::now(), 
                                                   "/map", 
                                                   "/odom")); */

    broadcaster.sendTransform(
                              tf::StampedTransform(tf::Transform(tf::Quaternion(orx,ory,orz,orw), tf::Vector3(posx,posy,posz)),
                                                   ros::Time::now(), 
                                                   "/odom", 
                                                   "/base_footprint"));
    /*broadcaster.sendTransform(tf::StampedTransform(tf::Transform(tf::createQuaternionFromRPY(0,-PI, 0), tf::Vector3(-0.35,-0.0225,0.3)),
                                                   ros::Time::now(), 
                                                   "/base_footprint", 
                                                    "/laser"));

    broadcaster.sendTransform(
                              tf::StampedTransform(tf::Transform(tf:: Quaternion(0,0,0,1), tf::Vector3(0,0.0,0.17)), 
                                                   ros::Time::now(), 
                                                   "/base_footprint", 
                                                   "/base_link"));*/
    broadcaster.sendTransform(
                              tf::StampedTransform(tf::Transform(tf:: createQuaternionFromRPY(0,23.5/180.0*PI,0 ), tf::Vector3(0,0,0.82)), 
                                                   ros::Time::now(), //0 , 0 , 0 //-0.4,0.2,0.53
                                                   "/base_footprint", 
                                                   "/camera_link"));
    /*broadcaster.sendTransform(
                              tf::StampedTransform(tf::Transform(tf:: createQuaternionFromRPY(0,0, (PI/2.0)+(pan_tf_angle)/180.0*PI), tf::Vector3(0.01, 0.0, 0.695)), 
                                                   ros::Time::now(), 
                                                   "/pan_tilt_link", 
                                                   "/pan_link")); //0, 0.0, .7 (basic amd tripod)
    broadcaster.sendTransform(
                              tf::StampedTransform(tf::Transform(tf:: createQuaternionFromRPY(0,-(tilt_tf_angle)/180.0*PI,0.), tf::Vector3(0.0, 0.0, 0.045)), 
                                                   ros::Time::now(), 
                                                   "/pan_link", 
                                                   "/tilt_link")); //0.01, -0.01, 0.045(tripod and basic) //0.0, 0.0, 0.045
    broadcaster.sendTransform(
                              tf::StampedTransform(tf::Transform(tf:: createQuaternionFromRPY(0,0,0), tf::Vector3(0.0, 0.0, 0.31)), //.0,.0,.03(on the tripod), basic(0.0, 0.1, 0.28)
                                                   ros::Time::now(), 
                                                   "/tilt_link", 
                                                   "/camera_link"));//+46 //0.0, 0.0, 0.28 //-0.50,46./180.0*PI,0.09*/
   /* broadcaster.sendTransform(
                              tf::StampedTransform(tf::Transform(tf:: createQuaternionFromRPY(0.,0.,0.), tf::Vector3(0.0, 0.0325, 0.255)), 
                                                   ros::Time::now(), 
                                                   "/tilt_link", 
                                                   "/camera_depth_optical_frame"));
    broadcaster.sendTransform(
                              tf::StampedTransform(tf::Transform(tf:: createQuaternionFromRPY(0.,0.,0.), tf::Vector3(0.0, 0, 0)), 
                                                   ros::Time::now(), 
                                                   "/camera_depth_frame", 
                                                   "/camera_depth_optical_frame"));
    broadcaster.sendTransform(
                              tf::StampedTransform(tf::Transform(tf:: createQuaternionFromRPY(0.,0.,0.), tf::Vector3(0.0, 0, 0)), 
                                                   ros::Time::now(), 
                                                   "/camera_depth_frame", 
                                                   "/camera_link"));
    broadcaster.sendTransform(
                              tf::StampedTransform(tf::Transform(tf:: createQuaternionFromRPY(0.,0.,0.), tf::Vector3(0.0, 0, 0)), 
                                                   ros::Time::now(), 
                                                   "/camera_depth_frame", 
                                                   "/camera_rgb_frame"));
    broadcaster.sendTransform(
                              tf::StampedTransform(tf::Transform(tf:: createQuaternionFromRPY(0.,0.,0.), tf::Vector3(0.0, 0, 0)), 
                                                   ros::Time::now(), 
                                                   "/camera_depth_frame", 
                                                 "/camera_rgb_optical_frame"));*/
                                                    
    ros::spinOnce();
    r.sleep();
  }
}
