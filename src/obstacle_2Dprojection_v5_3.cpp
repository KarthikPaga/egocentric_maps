#include <iostream>
#include <ros/ros.h>
#include <Eigen/Core>
#include <vector>
#include <fstream>
#include <iomanip>
//#include <Matrix.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
//#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
//#include <pcl/segmentation/extract_clusters.h>
#include <pcl/visualization/cloud_viewer.h>
//#include <pcl/common/centroid.h>
#include <pcl/registration/icp.h>
#include <pcl/point_cloud.h>
#include <pcl/common/transformation_from_correspondences.h>
//#include <boost/thread/thread.hpp>
#include <pcl/filters/project_inliers.h>
#include <pcl/keypoints/sift_keypoint.h>
#include <pcl/registration/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <math.h>
#include <cmath>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>
#include "pcl_ros/transforms.h"
#include <pcl_ros/point_cloud.h>
//#include "std_msgs/Float64.h"
#include "nav_msgs/Odometry.h"

ros::Publisher pubx;
ros::Publisher puby;
ros::Publisher pubz;
tf::TransformListener *listener;
tf::StampedTransform transform;

//void arrayCallback(const std_msgs::Float64::ConstPtr& odom_change);

const float pi = 3.14159;
const float rho_min = 0.3, rho_max = 4.3, theta_min = -pi, theta_max = pi, width = 20, height = 120; //variables as used in Pouria's map_builder_mod2.py theta-> height, rho -> width
const float delta_rho = 0.2, delta_theta = 0.05;
float p_mt = 0.2, p_mt_conj = 1-0.2, p_mt1_mt0 = 0.94, p_mt1_mt0_conj = 1-0.94;
int array[20][120];int x = 0, y = 0;
float abcissa =0, ordinate =0;
float rho_iterator = 0, theta_iterator =0, rho_calculator = 0, theta_calculator = 0.0;
float update = 0, slope = 5000;
float logistic = 0;
float l_t[20][126];
float x_t = 0.0; 
float x_past = 0.0;
float y_t = 0.0;
float y_past = 0.0;
float theta_t = 0.0;
float theta_past = 0.0;
float change[3];
float d_x = 0.0;
float d_y = 0.0;
int c =1;

float d_rho = 0.0, d_theta = 0.0, rho_new = 0.0, theta_new = 0.0;

 
pcl::PointCloud<pcl::PointXYZI> final_output;



void 
cloud_cb (const sensor_msgs::PointCloud2ConstPtr& input)
{
  p_mt = 0.5;
  p_mt_conj = 1-0.5;
  for (x = 0; x < 20; x++)
  {
   for (y = 0; y < 120; y++)
   {
    array[x][y] = std::numeric_limits<int>::quiet_NaN();
   }
  }
    sensor_msgs::PointCloud2 input1;
   
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_blob(new pcl::PointCloud<pcl::PointXYZRGB>);
  
    listener->waitForTransform("/base_footprint", (*input).header.frame_id, (*input).header.stamp, ros::Duration(5.0));
  pcl_ros::transformPointCloud("/base_footprint", *input, input1, *listener);
    
    pcl::fromROSMsg (input1, *cloud_blob);

  // Output Sensor MSG point cloud
  //sensor_msgs::PointCloud2 output_cloud;
  // Read in the cloud data
  int rho = 0, theta =0, count =0;
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::PointCloud<pcl::PointXYZI> cloud_visualizer;//(new pcl::PointCloud<pcl::PointXYZI>);
  //pcl::PointCloud<pcl::PointXYZI> final_output;
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZRGB>), cloud_f (new pcl::PointCloud<pcl::PointXYZRGB>), cloud_p (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::PCLPointCloud2::Ptr cloud_filtered_blob (new pcl::PCLPointCloud2);
  
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_projected (new pcl::PointCloud<pcl::PointXYZRGB>), cloud_polar (new pcl::PointCloud<pcl::PointXYZRGB>);  
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr plane_polar (new pcl::PointCloud<pcl::PointXYZRGB>), obstacle_polar (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_polar_blob (new pcl::PointCloud<pcl::PointXYZRGB>), outcloud_for_polar (new pcl::PointCloud<pcl::PointXYZRGB>), outcloud_from_plane (new pcl::PointCloud<pcl::PointXYZRGB>), outcloud_from_obstacle (new pcl::PointCloud<pcl::PointXYZRGB>);


  //pcl::PCLPointCloud2::Ptr cloud_filtered_blob (new pcl::PCLPointCloud2);
  //pcl::fromPCLPointCloud2 (*cloud_blob, *cloud);
  cloud = cloud_blob;
  //std::cout << "PointCloud before filtering has: " << cloud->points.size () << " data points." << std::endl; //*

  // Create the filtering object: downsample the dataset using a leaf size of 1cm
  pcl::VoxelGrid<pcl::PointXYZRGB> vg;
  
  vg.setInputCloud (cloud);
  vg.setLeafSize (0.01f, 0.01f, 0.01f);
  vg.filter (*cloud_filtered);
  //std::cout << "PointCloud after filtering has: " << cloud_filtered->points.size ()  << " data points." << std::endl; //*

  // Create the segmentation object for the planar model and set all the parameters
  pcl::SACSegmentation<pcl::PointXYZRGB> seg;
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_plane (new pcl::PointCloud<pcl::PointXYZRGB> ());
  //pcl::PCDWriter writer;
  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setMaxIterations (100);
  seg.setDistanceThreshold (0.02);
  pcl::ExtractIndices<pcl::PointXYZRGB> extract;
  int i=0, nr_points = (int) cloud_filtered->points.size ();
  while (cloud_filtered->points.size () > 0.3 * nr_points)
  {
    // Segment the largest planar component from the remaining cloud
    seg.setInputCloud (cloud_filtered);
    seg.segment (*inliers, *coefficients);
    if (inliers->indices.size () == 0)
    {
      std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
      break;
    }

    // Extract the planar inliers from the input cloud
    
    extract.setInputCloud (cloud_filtered);
    extract.setIndices (inliers);
    extract.setNegative (false);

    // Get the points associated with the planar surface
    extract.filter (*cloud_plane);
    //std::cout << "PointCloud representing the planar component: " << cloud_plane->points.size () << " data points." << std::endl;

    // Remove the planar inliers, extract the rest
    //pcl::ExtractIndices<pcl::PointXYZRGB> extract_obstacles;
    //extract_obstacles.setInputCloud (cloud_filtered);
    //extract_obstacles.setIndices (inliers);
    extract.setNegative (true);
    extract.filter (*cloud_f);
    cloud_filtered = cloud_f;
    //std::cout << "PointCloud representing the obstacles component: " << cloud_f->points.size () << " data points." << std::endl;
  }
  //std::cout << "out of while loop" << std::endl;
  int k = 0, nr_points_f = (int) cloud_plane->points.size ();
  pcl::IndicesPtr remaining_free_space (new std::vector<int>);
  remaining_free_space->resize (nr_points_f);
  for (std::vector<int>::iterator it = remaining_free_space->begin(); it != remaining_free_space->end(); ++it)
  {
    uint8_t r = 255, g = 255, b = 255;
    uint32_t rgb = ((uint32_t)r << 16 | (uint32_t)g << 8 | (uint32_t)b);
    cloud_plane->at(*it).rgb = *reinterpret_cast<float*>(&rgb);
  }
  //std::cout << "cloud_plane chnaged color" << std::endl;
  int l = 0, nr_points_o = (int) cloud_f->points.size ();
  pcl::IndicesPtr remaining_obstacle (new std::vector<int>);
  remaining_obstacle->resize (nr_points_o);
  for (std::vector<int>::iterator it = remaining_obstacle->begin(); it != remaining_obstacle->end(); ++it)
  {
    uint8_t r = 0, g = 0, b = 0;
    uint32_t rgb = ((uint32_t)r << 16 | (uint32_t)g << 8 | (uint32_t)b);
    cloud_f->at(*it).rgb = *reinterpret_cast<float*>(&rgb);
  }  
  //std::cout << "cloud_obstacle chnaged color" << std::endl;
  //projec obstacles on to ground plane
  pcl::ProjectInliers<pcl::PointXYZRGB> proj;
  proj.setModelType (pcl::SACMODEL_PLANE);
  proj.setInputCloud (cloud_f);
  proj.setModelCoefficients (coefficients);
  proj.filter (*cloud_projected);

//  const Eigen::Vector3f translate (0.5, 0.5, 1.5);
//  const Eigen::Quaternionf rotation (0.966, 0, 0, 0.2588);
//  pcl::transformPointCloud (*cloud_projected, *cloud, -translate, rotation);

  //tranformation to polar coordinates. there might be changes with assigning "0" to a coordinate, this is dependent on the fact as to which of the coordinate axis is required to be neglected.


  // Publish the planes we found.
  //std::cout << "projection done" << std::endl;
  pcl::PCLPointCloud2 outcloud_obstacle;
  pcl::PCLPointCloud2 outcloud;
  pcl::toPCLPointCloud2 (*cloud_projected, outcloud_obstacle);
  pcl::PCLPointCloud2 outcloud_plane;
  pcl::toPCLPointCloud2 (*cloud_plane, outcloud_plane);
  pcl::concatenatePointCloud (outcloud_obstacle, outcloud_plane, outcloud);
  pubx.publish (outcloud);
  

//std::cout << "set new names to the cloud data" << std::endl;
  //pcl::fromPCLPointCloud2 (outcloud, *outcloud_for_polar);
  //std::cout << "the problem was here" << std::endl;

///--------------this is not required as of now, but what this does is, it converts the concatenated point cloud data to polar coordinates---which as a whole we do not prefer to use 
  /*int j = 0;
  for (size_t j = 0; j < outcloud->points.size (); ++j)
  {
    cloud_polar->points[j].x = sqrt(((outcloud->points[j].x)*(outcloud->points[j].x)) + ((outcloud->points[j].y)*(outcloud->points[j].y)));  
    cloud_polar->points[j].y = atan2((outcloud->points[j].y), (outcloud->points[j].x));
    cloud_polar->points[j].z = 0;
    cloud_polar->points[j].rgb = outcloud->points[j].rgb;
    //cloud->points[i].x
  }
  std::cout << "converted outcloud to polar" << std::endl;*/
  //pcl::fromPCLPointCloud2 (outcloud_plane, *outcloud_from_plane);
  

  //int g = 0, nr_points_cloud_plane = (int) cloud_plane->points.size ();
  //pcl::IndicesPtr planar_indices (new std::vector<int>);
  //planar_indices->resize (nr_points_cloud_plane);
  //for (std::vector<int>::iterator it = planar_indices->begin(); it != planar_indices->end(); ++it)
  //{
//-----

  int g = 0;
  for (size_t g = 0; g < cloud_plane->points.size (); ++g)
  {
    abcissa = cloud_plane->points[g].x;
    ordinate = cloud_plane->points[g].y;
    //rho = plane_polar->points[g].x;
    //std::cout << "entered for loop-1" << std::endl;
    rho_iterator = sqrt(pow(abcissa,2) + pow(ordinate,2));  
    //std::cout << "1st stmnt done" << std::endl;
    theta_iterator = atan2(ordinate, abcissa);
    //std::cout << "2nd stmnt done" << std::endl;
    //plane_polar->points[g].z = 0;
    //plane_polar->points[g].rgb = cloud_plane->at(*it).rgb;
    cloud_plane->points[g].x = rho_iterator;
    cloud_plane->points[g].y = theta_iterator;
    cloud_plane->points[g].z = 0;
    
    //cloud->points[i].x
  }
  rho_iterator = 0;
  theta_iterator = 0;
  abcissa =0;
  ordinate = 0;
  //std::cout << "converted planar outcloud to polar" << std::endl;
  plane_polar = cloud_plane;
  //pcl::fromPCLPointCloud2 (outcloud_obstacle, *outcloud_from_obstacle);
  int h = 0;
  for (size_t h = 0; h < cloud_projected->points.size (); h++)
  {
    //std::cout << "entered for loop-2" << std::endl;
    abcissa = cloud_projected->points[h].x;
    ordinate = cloud_projected->points[h].y;
    //std::cout << "the problem is here and bug found" << std::endl;
    rho_iterator = sqrt(pow(abcissa,2) + pow(ordinate,2));  
    theta_iterator = atan2(ordinate, abcissa);
    //obstacle_polar->points[h].z = 0;
    //obstacle_polar->points[h].rgb = cloud_projected->points[h].rgb;
    cloud_projected->points[h].x = rho_iterator;
    cloud_projected->points[h].y = theta_iterator;
    cloud_projected->points[h].z = 0;
    //cloud->points[i].x
  }
  rho_iterator = 0;
  theta_iterator = 0;
  abcissa =0;
  ordinate = 0;
  obstacle_polar = cloud_projected;
  //std::cout << "converted obstacle outcloud to polar" << std::endl;
  std::cout << "started memory map--aggregation" << std::endl;
  //If we concatenate the point clouds at this instance, the process will become very costly later when you need to check if a given point 
  //is from plane or the obstacle
  /*j is associated with theta and i with rho and x is associated with rho and y with theta*/
  

  //int m = 0, nr_points_plane = (int) plane_polar->points.size ();
  //pcl::IndicesPtr plane_matter (new std::vector<int>);
  //plane_matter->resize (nr_points_plane);
  //for (std::vector<int>::iterator it = plane_matter->begin(); it != plane_matter->end(); ++it)
  //{
  int m = 0;
  for (size_t m = 0; m < plane_polar->points.size (); ++m)
  {
    //std::cout << "reached 1st phase of aggregator" << std::endl;
    theta = round(((plane_polar->points[m].y)-theta_min)/delta_theta);
    rho = round(width*((sqrt((plane_polar->points[m].x)-rho_min))/(sqrt(rho_max-rho_min))));

    array[rho][theta] = array[rho][theta] -1;
    //cout << array[rho][theta]  << "  " << "address:" << rho << "and" << theta <<"-|-";
  }
  //std::cout << "planar cloud done--aggregation" << std::endl;
  //int n = 0, nr_points_obstacle = (int) obstacle_polar->points.size ();
  //pcl::IndicesPtr obstacle_matter (new std::vector<int>);
  //obstacle_matter->resize (nr_points_obstacle);
  //for (std::vector<int>::iterator it = obstacle_matter->begin(); it != obstacle_matter->end(); ++it)
  //{
  int n = 0;
  for (size_t n = 0; n < obstacle_polar->points.size (); ++n)
  {
    theta = round(((obstacle_polar->points[n].y)-theta_min)/delta_theta);
    rho = round(width*((sqrt((obstacle_polar->points[n].x)-rho_min))/(sqrt(rho_max-rho_min))));

    array[rho][theta] = array[rho][theta] +1;
    //cout << array[rho][theta]  << "  " << "address:" << rho << "and" << theta <<"-|-";
  }
  //std::cout << "obstacle cloud done--aggregation" << std::endl; 
  /*for (x = 0; x < 20; x++)
  {
   for (y = 0; y < 120; y++)
   {
    cout << array[x][y]  << "  ";
    
   }
   cout << endl;
  }*/
  abcissa =0;
  ordinate = 0;
  count = 0;
  cloud_visualizer.width  = 126;
  cloud_visualizer.height = 126;
  cloud_visualizer.points.resize(cloud_visualizer.width * cloud_visualizer.height);
  //cloud_visualizer.header.stamp    = rospy.get_rostime()
  cloud_visualizer.header.frame_id = "/base_footprint";
  int a = 0, b=0;
  for (a = 0; a < 20; a++)
  {
   for (b = 0; b < 126; b++)
   {
    //std::cout << "entered 'the' for loop" << std::endl;
    abcissa = (rho_min + pow(a*(sqrt(rho_max-rho_min)/width), 2))*cos(theta_min+(b*delta_theta));
    ordinate = (rho_min + pow(a*(sqrt(rho_max-rho_min)/width), 2))*sin(theta_min+(b*delta_theta));
    //std::cout << "about to enter cloud_vis" << std::endl;
    cloud_visualizer.points[count].x = abcissa;
    cloud_visualizer.points[count].y = ordinate;
    //std::cout << "("<<abcissa<<","<<b*delta_theta<<","<<ordinate<<","<<array[a][b]<<") ";
    cloud_visualizer.points[count].z = 0;
    cloud_visualizer.points[count].intensity = array[a][b];
    count++ ;    
   }
   cout << endl;
   
  }
  //std::cout << "out of for loop" << std::endl;
  //pcl::visualization::CloudViewer viewer ("Simple Cloud Viewer");
  //viewer.showCloud (cloud_visualizer);
  //while (!viewer.wasStopped ())
  //{
  //}
  pcl::PCLPointCloud2 outcloud_tmp_visual;
  pcl::toPCLPointCloud2 (cloud_visualizer, outcloud_tmp_visual);
  puby.publish (outcloud_tmp_visual); 
  /*---*****final mp is no longer indicated from this pipleline, the map call back does this, with the motion updates integrated to it.
  abcissa =0;
  ordinate = 0;
  a = 0, b=0;
  count = 0;
  final_output.width  = 126;
  final_output.height = 126;
  final_output.points.resize(final_output.width * final_output.height);
  final_output.header.frame_id = "/base_footprint";
  for (a = 0; a < 20; a++)
  {
   for (b = 0; b < 126; b++)
   {
    //std::cout << "entered 'the' for loop" << std::endl;
    abcissa = (rho_min + pow(a*(sqrt(rho_max-rho_min)/width), 2))*cos(theta_min+(b*delta_theta));
    ordinate = (rho_min + pow(a*(sqrt(rho_max-rho_min)/width), 2))*sin(theta_min+(b*delta_theta));
    //std::cout << "about to enter cloud_vis" << std::endl;
    final_output.points[count].x = abcissa;
    final_output.points[count].y = ordinate;
    //std::cout << "("<<abcissa<<","<<b*delta_theta<<","<<ordinate<<","<<array[a][b]<<") ";
    final_output.points[count].z = 0;
    final_output.points[count].intensity = l_t[a][b];
    logistic = (1/(1+(exp (-1 * slope * array[a][b]))));
    logistic = logistic + 0.00000001;
    if (logistic >= 0.00000001 && logistic <=0.99999999)
    {

     update = log(logistic)-log(1-logistic) - log(p_mt) + log(p_mt_conj) + log(p_mt1_mt0)- log(p_mt1_mt0_conj) + l_t[a][b];
     if ( std::isinf( update ))
     {
      std::cout << "("<<update<<","<<log(logistic)<<","<<log(1-logistic)<<","<<log(p_mt)<<","<<","<<log(p_mt_conj)<<","<<log(p_mt1_mt0)<<","<<log(p_mt1_mt0_conj)<<","<<l_t[a][b]<<") "<<"the logistic value is:"<<logistic<< std::endl;
     
     }
     l_t[a][b] = update;
     final_output.points[count].intensity = l_t[a][b];
    }
    //l_t_previous = l_t;
    count++ ;    
   }
   cout << endl;
   
  }
  //std::cout << "out of for loop" << std::endl;
  //pcl::visualization::CloudViewer viewer ("Simple Cloud Viewer");
  //viewer.showCloud (cloud_visualizer);
  //while (!viewer.wasStopped ())
  //{
  //}
  pcl::PCLPointCloud2 outcloud_visual;
  pcl::toPCLPointCloud2 (final_output, outcloud_visual);
  pubz.publish (outcloud_visual);
  */
} 

//the mapcallback function builds the map by integrating the motion updates navigational msgs from odometry.
void mapCallback(const nav_msgs::Odometry odomdata)
{
  x_t = odomdata.pose.pose.position.x;
  y_t = odomdata.pose.pose.position.y;
  theta_t = atan2(y_t, x_t);

  d_x = x_t - x_past;
  d_y = y_t - y_past;
  d_theta = theta_t - theta_past;
  change[0] = d_x;
  change[1] = d_y;
  change[2] = d_theta;
  final_output.width  = 126;
  final_output.height = 126;
  final_output.points.resize(final_output.width * final_output.height);
  final_output.header.frame_id = "/base_footprint";

  // we check for the deviation that can be entertained, these values have to check by trial and error, in this case 0.25 was chosen randomly
  if(change[0] < 0.25 && change[1] < 0.25 && change[2] < 0.25)
  {
  abcissa =0;
  ordinate = 0;
  int a = 0, b=0;
  int count = 0;

  for (a = 0; a < 20; a++)
  {
   for (b = 0; b < 126; b++)
   {
    //std::cout << "entered 'the' for loop" << std::endl;
    abcissa = (rho_min + pow(a*(sqrt(rho_max-rho_min)/width), 2))*cos(theta_min+(b*delta_theta));
    ordinate = (rho_min + pow(a*(sqrt(rho_max-rho_min)/width), 2))*sin(theta_min+(b*delta_theta));
    //std::cout << "about to enter cloud_vis" << std::endl;
    final_output.points[count].x = abcissa;
    final_output.points[count].y = ordinate;
    //std::cout << "("<<abcissa<<","<<b*delta_theta<<","<<ordinate<<","<<array[a][b]<<") ";
    final_output.points[count].z = 0;
    final_output.points[count].intensity = l_t[a][b];
    logistic = (1/(1+(exp (-1 * slope * array[a][b]))));
    logistic = logistic + 0.00000001;
    if (logistic >= 0.00000001 && logistic <=0.99999999)
    {

     update = log(logistic)-log(1-logistic) - log(p_mt) + log(p_mt_conj) + log(p_mt1_mt0)- log(p_mt1_mt0_conj) + l_t[a][b];
     if ( std::isinf( update ))
     {
      std::cout << "("<<update<<","<<log(logistic)<<","<<log(1-logistic)<<","<<log(p_mt)<<","<<","<<log(p_mt_conj)<<","<<log(p_mt1_mt0)<<","<<log(p_mt1_mt0_conj)<<","<<l_t[a][b]<<") "<<"the logistic value is:"<<logistic<< std::endl;
     
     }
     l_t[a][b] = update;
     final_output.points[count].intensity = l_t[a][b];
    }
    //l_t_previous = l_t;
    count++ ;    
   }
   cout << endl;
   
  }
  }
  else if(change[0] >= 0.25 || change[1] >= 0.25 || change[2] >= 0.25)
  {
  abcissa =0;
  ordinate = 0;
  int a = 0, b=0;
  int count = 0;
  rho_calculator = 0;
  theta_calculator = 0;
  
  for (a = 0; a < 20; a++)
  {
   for (b = 0; b < 126; b++)
   {
    //std::cout << "entered 'the' for loop" << std::endl;
    rho_calculator = rho_min + pow(a*(sqrt(rho_max-rho_min)/width), 2);
    theta_calculator = theta_min+(b*delta_theta);
    d_rho = sqrt(((pow(change[0],2))+(pow(change[1],2))));
    d_theta = atan2(change[1], change[0]);
    rho_new = sqrt(pow(rho_calculator,2) + pow(d_rho,2) - (2*rho_calculator*d_rho*cos(theta_calculator-d_theta)));
    theta_new = atan2(((rho_calculator*cos(theta_calculator))-(d_rho*cos(d_theta))),((rho_calculator*sin(theta_calculator))-(d_rho*sin(d_theta))));
    abcissa = (rho_new)*cos(theta_new);
    ordinate = (rho_new)*sin(theta_new);
    //std::cout << "about to enter cloud_vis" << std::endl;
    if (rho_new <0.3 || rho_new >0.43 || theta_new > pi || theta_new < -pi)
    {
     break;
    }
    final_output.points[count].x = abcissa;
    final_output.points[count].y = ordinate;
    //std::cout << "("<<abcissa<<","<<b*delta_theta<<","<<ordinate<<","<<array[a][b]<<") ";
    final_output.points[count].z = 0;
    final_output.points[count].intensity = l_t[a][b];
    logistic = (1/(1+(exp (-1 * slope * array[a][b]))));
    logistic = logistic + 0.00000001;
    if (logistic >= 0.00000001 && logistic <=0.99999999)
    {

     update = log(logistic)-log(1-logistic) - log(p_mt) + log(p_mt_conj) + log(p_mt1_mt0)- log(p_mt1_mt0_conj) + l_t[a][b];
     if ( std::isinf( update ))
     {
      std::cout << "("<<update<<","<<log(logistic)<<","<<log(1-logistic)<<","<<log(p_mt)<<","<<","<<log(p_mt_conj)<<","<<log(p_mt1_mt0)<<","<<log(p_mt1_mt0_conj)<<","<<l_t[a][b]<<") "<<"the logistic value is:"<<logistic<< std::endl;
     
     }
     l_t[a][b] = update;
     final_output.points[count].intensity = l_t[a][b];
    }
    //l_t_previous = l_t;
    count++ ;    
   }
   cout << endl;
   
  }
  }

  pcl::PCLPointCloud2 outcloud_visual;
  pcl::toPCLPointCloud2 (final_output, outcloud_visual);
  pubz.publish (outcloud_visual);
}


int
main (int argc, char** argv)
{
  //float p_mt = 0.2, p_mt_conj = 0.8;
if (c == 1)
{
for(int x = 0; x < 20; x++)
{
 for(int y = 0; y < 120; y++)
 {
  
  l_t[x][y] = log(p_mt)-log(p_mt_conj); 
 }
}
c++;
}
  
  float delta_rho = 0.0, delta_theta = 0.0, rho_new = 0.0, theta_new = 0.0;
  for (x = 0; x < 20; x++)
  {
   for (y = 0; y < 120; y++)
   {
    array[x][y] = std::numeric_limits<int>::quiet_NaN();
    //l_t[x][y] = log(p_mt)-log(p_mt_conj); --there is some problem if you keep this statement here, because the l_t[][] is renewed for every frame
   }
  }
  // Initialize ROS
  ros::init (argc, argv, "obstacle_projection");
  ros::NodeHandle nh;
  listener= new tf::TransformListener();
  // Create a ROS subscriber for the input point cloud
  //before a new frame comes in the position of the robot from the previous frame has to be stored
  x_past = x_t;
  y_past = y_t;
  theta_past = theta_t;
  ros::Subscriber sub = nh.subscribe ("input", 1, cloud_cb);
  ros::Subscriber sub1 = nh.subscribe("odom", 1, mapCallback);


  // Create a ROS publisher for the output point cloud
  pubx = nh.advertise<pcl::PCLPointCloud2> ("obstacle_projection", 10);
  puby = nh.advertise<pcl::PCLPointCloud2> ("map_visual", 1);
  pubz = nh.advertise<pcl::PCLPointCloud2> ("map_egocentric", 1);
  //puby = nh.advertise<pcl::PointCloud<pcl::PointXYZ> > ("map_visual", 1);
  // Spin
  ros::spin ();
}
