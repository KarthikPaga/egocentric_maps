﻿Instructions 
we use rosmake workspace not the catkin counterpart.****** USE THE ROBUILD INSTRUCTIONS FOR INSTALLING ANYTHING *******
Inatalling PCl for ROS_hydo (hydro -> rosbuild) http://wiki.ros.org/pcl/Tutorials
or 
follow the these instructions //my_pcl_tutorial is the name of the package your the creating, you might want to keep a local package to avoid conflicts if any in the repository
$ roscore
$ cd /....rosmake workspace.....
$ roscreate-pkg my_pcl_tutorial pcl_conversions pcl_ros roscpp sensor_msgs
copy the file called src/example.cpp from the cloned repo
Edit the CMakeLists.txt file in your newly created package and add:
rosbuild_add_executable (example src/example.cpp)
now build the package
$rosmake my_pcl_tutorial

now build the cloned repository 


# assuming that you have installed pcl for ros
1. This version of the code is written for ros hydro on ubuntu 12.04 32bit working environment
2. clone the git repository, 
3. Build the “egocentric_maps” package in your rosmake workspace
   $ rosmake egocentric maps
   
1. $ roscore
2. $ roslaunch openni_launch openni.launch
3. $ rosrun egocentric_maps tf_broadcaster_2
4. $ rosrun egocentric_maps obstacle_2Dprojection_v5_3 input:=/camera/depth_registered/points
5. $ rosrun rviz rviz
